const path="/person";
const {Router}= require('express');
const { getPersonByIdController, getAllPersonController, deletePersonByIdController, createPersonController, updatePersonIdController  } = require('../controllers/person.controller');
const router=Router();   


//LIST
router.get(`${path}`, getAllPersonController)
//CHECK ID
router.get(`${path}/:id`, getPersonByIdController)
//DELETE ID
router.delete(`${path}/:id`, deletePersonByIdController)
//CREATE
router.post(`${path}`,createPersonController)
//UPDATE
router.put(`${path}/:id`, updatePersonIdController)
module.exports={personRouter : router}

