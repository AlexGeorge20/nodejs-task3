const fs = require("fs");
function read() {
  const data = fs.readFileSync("sample.json", { encoding: "utf-8" });
  return JSON.parse(data);
}
function write(temp) {
  const writing = fs.writeFileSync("sample.json", JSON.stringify(temp));
  console.log("UpdatedArrywrite()", temp);
}
// GET-id
function getPersonByIdService(id){
    let data="";
    console.log("Params", id);
    const temp = read();
    let idcheck = temp.data.find((element) => element.id == id);
    console.log("ID Check", idcheck);
    if (idcheck) {
      console.log("ID found" + " " + idcheck.name + " " + idcheck.id);
      data="ID found" + " " + idcheck.name + " " + "ID: " + idcheck.id;
    } else {
      console.log("ID not found for check");
      data="ID not found";
    }
    return data
}
//DELETE id
function deletePersonbyIdService(id){
    let data="";
    console.log("Params", id);
    const temp = read();
    let idcheck = temp.data.find((element) => element.id == id);
  
    if (idcheck) {
      console.log("IDCHECK-found", idcheck);
      let deletedarry = temp.data.filter((ele) => ele.id != idcheck.id);
      const obj = { data: deletedarry };
    //   res.send(obj);
      write(obj);
      data=obj
    } else {
    //   console.log("ID not found");
    data="ID not found";
    }
    return data;
}
// CREATE
function createPersonService(name){
    const temp =read();
    temp.data.push({ name: name, id:  Date.now() });
    console.log("CREATEd", temp);
    write(temp);
    return temp
}
//UPDATE
function updatePersonIdService(id,newName){
    let data=""
    const temp = read();
    let idcheck = temp.data.find((element) => element.id == id);
    if(idcheck){
        console.log("ID found ", idcheck);
        idcheck.name = newName;
        write(temp);
        console.log("ID found, UPDAted arr", temp);
        data=("ID found, UPDAted arr", temp);

    }else {
          console.log("ID not found, nothing to update");
        data="ID not found, nothing to update";
        }
        return data

}

module.exports={ getAllPersonsService : read, getPersonByIdService,
     deletePersonbyIdService, createPersonService, updatePersonIdService }
