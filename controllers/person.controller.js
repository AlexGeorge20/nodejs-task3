const {  getAllPersonsService, getPersonByIdService, deletePersonbyIdService,createPersonService, updatePersonIdService } = require("../services/person.service");


const getAllPersonController= (req, res) => {
    const temp = getAllPersonsService();
    res.status(201).send(temp);
    // console.log("READ", temp);
  }

 const getPersonByIdController= (req, res) => {
   const id=req.params.id;
   const response= getPersonByIdService(id);
   res.send(response)
  }

  const deletePersonByIdController = (req, res) => {
      const id= req.params.id;
   const response= deletePersonbyIdService(id)
   res.send(response);
  } 

  const createPersonController=  (req, res) => {
    const nametoadd= req.body.name;
    const response= createPersonService (nametoadd)
    res.send(response);
  }

  const updatePersonIdController= (req, res) => {
    const id= req.params.id;
    const name= req.body.name;
    const response= updatePersonIdService(id,name);
    res.send(response);

  }

  module.exports= { getAllPersonController ,getPersonByIdController,
    deletePersonByIdController,createPersonController ,updatePersonIdController }