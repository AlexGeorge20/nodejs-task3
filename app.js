const fs = require("fs");
function read() {
  const data = fs.readFileSync("sample.json", { encoding: "utf-8" });
  return data;
}
function write(temp) {
  const writing = fs.writeFileSync("sample.json", JSON.stringify(temp));
  console.log("UpdatedArrywrite()", temp);
}

const express = require("express");
const { personRouter } = require("./routes/person.route");
const app = express();

app.use(express.json());
app.use(personRouter)

module.exports= {app:app};
